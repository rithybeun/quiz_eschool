package com.example.mobilequiz.fragment

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.mobilequiz.R
import com.example.mobilequiz.helper.SQLiteHelper
import com.example.mobilequiz.models.StudentModels
import com.example.mobilequiz.utils.LodingDialog

class InsertRecord : AppCompatActivity() {
    private lateinit var txtID: EditText
    private lateinit var txtName: EditText
    private lateinit var txtSex: EditText
    private lateinit var txtSubject: EditText
    private lateinit var btnInsertStudent: Button
    private lateinit var sqlHelper: SQLiteHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_insert_record)

        initView()
        val bundle : Bundle =  intent.extras as Bundle
        val valueBtn = bundle.get("btnValue")
        val idStudent = bundle.get("idStudent")
        val nameStudent = bundle.get("nameStudent")
        val sexStudent = bundle.get("sexStudent")
        val subjectStudent = bundle.get("subjectStudent")

        //** Use for check Condition Is Update Or Create with the same UI*/
        if(valueBtn == "Update"){
            btnInsertStudent.text = valueBtn.toString()
            txtName.setText(nameStudent.toString())
            txtSex.setText(sexStudent.toString())
            txtSubject.setText(subjectStudent.toString())
        }
        //else{
//            btnInsertStudent.text = "Insert"
//        }
        sqlHelper = SQLiteHelper(this)

        //** This Button Use for Click On Update Or Add Student */
        btnInsertStudent.setOnClickListener {
            if(valueBtn == "Update"){
                updateStudent(idStudent as Int)
            }else
           addStduent()

        }


    }

    private fun initView() {
        txtName = findViewById(R.id.txtNameStudent)
        txtSex = findViewById(R.id.txtAgeStudent)
        txtSubject = findViewById(R.id.txtSubjectStudent)
        btnInsertStudent = findViewById(R.id.btnInsertStudent)
    }
    //** This Function use for add new student */
    private fun addStduent() {
        val handler = Handler()
        val name = txtName.text.toString()
        val sex = txtSex.text.toString()
        val sub = txtSubject.text.toString()
        val loading = LodingDialog(this)

        //* Use for check value is Empty or not */
        if ( name.isEmpty() || sex.isEmpty() || sub.isEmpty()) {
            btnInsertStudent.isEnabled = false
            handler.postDelayed({
                btnInsertStudent.isEnabled = true
            }, 2200)
            Toast.makeText(
                this,
                "Please Enter Required Field",
                Toast.LENGTH_SHORT
            ).show()

        } else {
            val stu = StudentModels(getLastRecordStudent()+1, name, sex, sub)
            val status = sqlHelper.insertStudent(stu)
            if (status > -1) {
                loading.startLoding()
//                processLoding.visibility = View.VISIBLE
                Toast.makeText(this, "Insert Success", Toast.LENGTH_SHORT).show()
                println("Success")
                ClearText()
                val handler =  Handler()
                handler.postDelayed({
//                    processLoding.visibility = View.GONE
                    loading.isDismiss()
                    finish()},1500)
            } else {
                loading.isDismiss()
                Toast.makeText(this, "Record Not Saved", Toast.LENGTH_SHORT).show()
            }

        }
    }
    //** This Function use for update student */
    private fun updateStudent(idStud : Int){
        val id = idStud
        val nameUpdate = txtName.text.toString()
        val sexUpdate = txtSex.text.toString()
        val subUpdate = txtSubject.text.toString()
        val loading = LodingDialog(this)
        //* Use for check value is Empty or not */
        if (id==null || nameUpdate.isEmpty() || sexUpdate.isEmpty() || subUpdate.isEmpty()) {
            Toast.makeText(this, "Record Not Changed", Toast.LENGTH_SHORT).show()
        } else {
            val stu = StudentModels(id, nameUpdate, sexUpdate, subUpdate)
            val status = sqlHelper.updateStudent(stu)
            if (status > -1) {

                loading.startLoding()

                ClearText()
                val handler =  Handler()
                handler.postDelayed({
                    loading.isDismiss()
                    Toast.makeText(this, "Update Success", Toast.LENGTH_SHORT).show()
                    println("Success")
                    finish()},1500)

            } else {
                Toast.makeText(this, "Update Failed", Toast.LENGTH_SHORT).show()
                println("Failed")
            }

        }
    }
    //** This Function Use for get max id of students record */
    fun getLastRecordStudent(): Int {
        var getId = sqlHelper.getLastId()
        return getId.id
    }
    //** This Function use for clear text*/
    fun ClearText() {

        txtName.setText("")
        txtSex.setText("")
        txtSubject.setText("")
    }
}
package com.example.mobilequiz

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.mobilequiz.fragment.InsertRecord
import com.example.mobilequiz.helper.SQLiteHelper
import com.example.mobilequiz.models.StudentModels
import com.example.mobilequiz.models.TableRowAdapter
import com.example.mobilequiz.utils.LodingDialog


class MyStudentFragment : Fragment() {
    private lateinit var sqlHelper: SQLiteHelper
    private lateinit var tableRecyclerView: RecyclerView
    private lateinit var tableRowAdapter: TableRowAdapter
    private lateinit var btnInsertStudent: Button
    override fun onResume() {
        super.onResume()
        getAllStduent()
        tableRowAdapter.notifyDataSetChanged()
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        //** Use for call Sqlite */
        sqlHelper = SQLiteHelper(context!!)
        val view = inflater.inflate(R.layout.students_info, container, false)
        tableRecyclerView = view!!.findViewById(R.id.recyclerViewStudent)
        initRecycle()
        getAllStduent()

        //** Use for callback function to action process delete student by ID*/
        tableRowAdapter?.setOnClickDeleteItems {
            deleteStudentById(it.id,context as Activity)
        }

        //** Use for callback functio to action process update student by ID */
        tableRowAdapter?.setOnClickUpdateItems {
            updateStudentById(StudentModels(it.id,it.nameStudent,it.ageStudent,it.subjectStudent))

        }
        btnInsertStudent = view!!.findViewById(R.id.btnCreateStudent)

        //** Use for Route to Insert Page */
        btnInsertStudent.setOnClickListener {
            var intent =  Intent(context, InsertRecord::class.java)
            intent.putExtra("valueBtn","Create")
            requireActivity().run {
                startActivity(
                    intent,
                )
            }
        }
        return view
    }
    //** This function use for updateStudent By ID*/
    private fun updateStudentById(stu : StudentModels) {
        println("Update")
        if (stu.id == null) return

        //** Use for sent Data to InsertRecord Page */
        var intent =  Intent(context, InsertRecord::class.java)
        intent.putExtra("btnValue","Update")
        intent.putExtra("idStudent",stu.id)
        intent.putExtra("nameStudent",stu.nameStudent)
        intent.putExtra("sexStudent",stu.ageStudent)
        intent.putExtra("subjectStudent",stu.subjectStudent)

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        requireActivity().run {
            startActivity(
                intent,
            )
        }


    }
    //** This Function use for delete Student By Id */
    private fun deleteStudentById(id: Int,activity: Activity) {
        var loading : LodingDialog = LodingDialog(activity)
        println("DFDSF $id")
        //**  Use for check condition if id equal null*/
        if (id == null) return
        val builder = AlertDialog.Builder(context)
        builder.setMessage("Do you want to delete this record?")
        builder.setCancelable(true)
        //** Click Yes if you want to delete it*/
        builder.setPositiveButton("Yes") { dialog, _ ->
            loading.startLoding()
            dialog.dismiss()
            val handler =  Handler()
            handler.postDelayed({
//                    processLoding.visibility = View.GONE
                sqlHelper.deleteStudentById(id)
                getAllStduent()
                tableRowAdapter.notifyDataSetChanged()
                loading.isDismiss()
              },1500)


        }
        builder.setNegativeButton("No") { dialog, _ ->
            dialog.dismiss()
        }
        var alert = builder.create()
        alert.show()
    }

    private fun initRecycle() {
        tableRowAdapter = TableRowAdapter()
        tableRecyclerView.layoutManager = LinearLayoutManager(context)
        tableRecyclerView.adapter = tableRowAdapter

    }

    //** This Function Use for get all Student*/
    private fun getAllStduent() {
        var stuList = sqlHelper.getAllStudent()

        println("hehe ${stuList.size}")
        tableRowAdapter?.addStudentItem(stuList)
    }
}

package com.example.mobilequiz.fragment

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.mobilequiz.R
import com.example.mobilequiz.constraint.LoginStatus
import com.example.mobilequiz.models.TeacherModels
import com.example.mobilequiz.utils.LodingDialog

class LoginActivity : AppCompatActivity() {
    private lateinit var txtID: EditText
    private lateinit var txtPassword: EditText
    private lateinit var btnLogin: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var wrongPassword: Boolean = false
        setContentView(R.layout.activity_login)
        initView()
        val listTeacher = ArrayList<TeacherModels>()
        initList(listTeacher)
        val loading = LodingDialog(this)
        val handler = Handler()
        var loginStatus: LoginStatus = LoginStatus.Init

        //** This Button use for Login with Phone And pasword */
        btnLogin.setOnClickListener {

            //** Use for check text is empty or not */

            if (txtID.text.isNotEmpty() && txtPassword.text.isNotEmpty()) {
                 val phone = txtID.text.toString()
                val password = txtPassword.text.toString()
                println("Phone $phone")
                println("Password $password")
                loading.startLoding()
                //** Use for Check value is correct or incorret */
                for (i in listTeacher) {
                    if (i.phoneNumber.contains(phone) && i.password.contains(password)) {
                        loginStatus = LoginStatus.Success
                        wrongPassword = false
                        handler.postDelayed({
                            val intent = Intent(this, AfterLoginActivity::class.java)
                            intent.putExtra("nameTeacher", i.name)
                            loading.isDismiss()
                            startActivity(intent)
                            finish()
                        }, 1500)
                    } else {
                        loginStatus = LoginStatus.Incorrect
                        wrongPassword = true;
                    }
                }
            } else {
                loginStatus = LoginStatus.Empty
            }
            //** Use for check account is incorret that will show */
            if (loginStatus == LoginStatus.Incorrect) {
                handler.postDelayed({
                    loading.isDismiss()
                    Toast.makeText(this, "Incorrect Username Or Password", Toast.LENGTH_SHORT)
                        .show()
                }, 1500)
            }
            //** Use for check text edit has value or not */
            else if (loginStatus == LoginStatus.Empty) {
                btnLogin.isEnabled = false
                handler.postDelayed({
                    btnLogin.isEnabled = true
                }, 2200)
                val toast = Toast.makeText(
                    this,
                    "Please Input Phone Number and Password",
                    Toast.LENGTH_SHORT
                ).show()

            }
        }

    }

    private fun initView() {
        txtID = findViewById(R.id.txtPhoneNumber)
        txtPassword = findViewById(R.id.txtPassword)
        btnLogin = findViewById(R.id.btnLoginFragment)
    }

    //** This functino use for store information of teacher for use to login
    //   Using with ArrayList
    // */
    private fun initList(arrayList: ArrayList<TeacherModels>) {

        arrayList.add(TeacherModels("011223344", "123@abc", "Rattanka"))
        arrayList.add(TeacherModels("012345678", "121314", "Rithy"))

    }



}
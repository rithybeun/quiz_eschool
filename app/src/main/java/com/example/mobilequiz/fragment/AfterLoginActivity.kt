package com.example.mobilequiz.fragment

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import com.example.mobilequiz.MyStudentFragment
import com.example.mobilequiz.PostFragment
import com.example.mobilequiz.R
import com.example.mobilequiz.SharingFragment
import com.example.mobilequiz.databinding.ActivityAfterLoginBinding

class AfterLoginActivity : AppCompatActivity() {
    lateinit var pageView: ViewPager
    lateinit var tabLayout: TabLayout
    lateinit var textNameTeacher : TextView
    private lateinit var binding: ActivityAfterLoginBinding
    private var isSelectFirst : Boolean = true;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAfterLoginBinding.inflate(layoutInflater)


        setContentView(binding.root)
        pageView = findViewById(R.id.viewPage)
        tabLayout = findViewById(R.id.tabLayout)
        textNameTeacher = findViewById(R.id.txtNameTeacher)
        //** User for get value from another page */
        val bundle : Bundle =  intent.extras as Bundle
        val valueBtn = bundle.get("nameTeacher")
        textNameTeacher.setText("សួរស្តី ${valueBtn.toString()}")


        tabLayout!!.addTab(tabLayout!!.newTab().setText("My Student"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Sharing"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Post"))
        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL


        val adapter = MyAdapter(this, supportFragmentManager, tabLayout!!.tabCount)

        pageView!!.adapter = adapter
        pageView!!.currentItem=1
        pageView!!.addOnPageChangeListener(
            TabLayout.TabLayoutOnPageChangeListener(tabLayout)
        )
        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            //** User for tab to switching page*/
            override fun onTabSelected(tab: TabLayout.Tab) {
                pageView!!.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                println("UnSelected")
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                if (isSelectFirst){
                    pageView!!.currentItem = 0
                    isSelectFirst = false
                }

            }

        })

    }

    class MyAdapter(
        private val myContext: Context,
        fm: FragmentManager,
        internal var totalTabs: Int
    ) : FragmentPagerAdapter(fm) {
        //** User For Switching From One Page To Another Page */
        // this is for fragment tabs
        override fun getItem(position: Int): Fragment? {
            when (position) {
                0 -> {
                    //  val homeFragment: HomeFragment = HomeFragment()
                    return MyStudentFragment()
                }
                1 -> {
                    return SharingFragment()
                }
                2 -> {
                    // val movieFragment = MovieFragment()
                    return PostFragment()
                }
                else -> return null
            }
        }

        // this counts total number of tabs
        override fun getCount(): Int {
            return totalTabs
        }


    }
}
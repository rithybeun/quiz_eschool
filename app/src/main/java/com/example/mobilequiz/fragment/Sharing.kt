package com.example.mobilequiz

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import com.example.mobilequiz.constraint.ListPost
import com.example.mobilequiz.models.MyAdapterNewFeed
import com.example.mobilequiz.models.NewFeedModel


class SharingFragment : Fragment(){
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view=inflater.inflate(R.layout.fragment_sharing, container, false)
        val listView = view!!.findViewById<ListView>(R.id.listViewSharing)
        var list = mutableListOf<NewFeedModel>()
        var listPost : ListPost = ListPost()

        //** Use for Loop add news feed to dispaly */
        for(dd in listPost.listPostFun(listPost.arrayList)){
            list.add(dd)
        }

        val myListAdapter = MyAdapterNewFeed(container!!.context,R.layout.list_item,list)
        listView.adapter = myListAdapter


        return view
    }

}


package com.example.mobilequiz.helper

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.mobilequiz.models.StudentModels

class SQLiteHelper(context: Context) : SQLiteOpenHelper(context,DATABASE_NAME, null,DATABASE_VERSION) {

    companion object {
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "student.db"
        private const val TBL_STUDENT = "tbl_student"
        private const val ID = "id"
        private const val NAME = "name"
        private const val SEX = "sex"
        private const val SUBJECT = "subject"
    }
    //** This function use for create table in localstorage (SQLITE)*/
    override fun onCreate(db: SQLiteDatabase?) {
        val createTableStudent =
            ("CREATE TABLE " + TBL_STUDENT + "(" + ID + " INTEGER PRIMARY KEY," + NAME + " TEXT," + SEX + " TEXT," + SUBJECT + " TEXT)")
        db?.execSQL(createTableStudent)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TBL_STUDENT")
        onCreate(db)
    }

    //** This function use for insert data of student to local storage (SQLITE) */
    fun insertStudent(stu: StudentModels): Long {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(ID, stu.id)
        contentValues.put(NAME, stu.nameStudent)
        contentValues.put(SEX, stu.ageStudent)
        contentValues.put(SUBJECT, stu.subjectStudent)
        val success = db.insert(TBL_STUDENT,null,contentValues)
        db.close()
        return success
    }

    //** This function use for query all data of student from local storage (SQLITE) */
    @SuppressLint("Range")
    fun getAllStudent() :ArrayList<StudentModels>{
        val stdList: ArrayList<StudentModels> = ArrayList()
        val selectQuery = "SELECT * FROM $TBL_STUDENT"
        val  db = this.readableDatabase
        val cursor : Cursor?
        try{
            cursor = db.rawQuery(selectQuery,null)
        }catch (e:Exception){
            e.printStackTrace()
            db.execSQL(selectQuery)
            return  arrayListOf()
        }
        var id : Int
        var nameStudent : String
        var sexStudent :String
        var subjectStudent :String
        if(cursor.moveToFirst()){
            do {
                id = cursor.getInt(cursor.getColumnIndex("$ID"))
                nameStudent = cursor.getString(cursor.getColumnIndex("$NAME"))
                sexStudent = cursor.getString(cursor.getColumnIndex("$SEX"))
                subjectStudent = cursor.getString(cursor.getColumnIndex("$SUBJECT"))

                val std = StudentModels(id,nameStudent,sexStudent,subjectStudent)
                stdList.add(std)
            }while (cursor.moveToNext())
        }
        return stdList
    }

    //** This function use for delete data of student from local storage (SQLITE) */
    fun deleteStudentById(id:Int):Int{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(ID,id)
        val success = db.delete(TBL_STUDENT,"id=$id",null)
        db.close()
        return success
    }

    //** This function use for update data of student from local storage (SQLITE) */
    fun updateStudent(stu:StudentModels) :Int{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(ID, stu.id)
        contentValues.put(NAME, stu.nameStudent)
        contentValues.put(SEX, stu.ageStudent)
        contentValues.put(SUBJECT, stu.subjectStudent)
        val success = db.update(TBL_STUDENT,contentValues,"id=" + stu.id,null)
        db.close()
        return success
    }

    //** This function use for get Last ID of data student from local storage (SQLITE) */
    @SuppressLint("Range")
    fun getLastId() :StudentModels{
        var std:StudentModels = StudentModels()
        val selectQuery = "SELECT * FROM $TBL_STUDENT WHERE $ID = (SELECT MAX($ID) FROM $TBL_STUDENT) "
        val  db = this.readableDatabase
        val cursor : Cursor?
        try{
            cursor = db.rawQuery(selectQuery,null)
        }catch (e:Exception){
            e.printStackTrace()
            db.execSQL(selectQuery)
            return  StudentModels()
        }
        var id : Int
        var nameStudent : String
        var sexStudent :String
        var subjectStudent :String
        if(cursor.moveToFirst()){
            do {
                id = cursor.getInt(cursor.getColumnIndex("$ID"))
                nameStudent = cursor.getString(cursor.getColumnIndex("$NAME"))
                sexStudent = cursor.getString(cursor.getColumnIndex("$SEX"))
                subjectStudent = cursor.getString(cursor.getColumnIndex("$SUBJECT"))

                 std = StudentModels(id,nameStudent,sexStudent,subjectStudent)

            }while (cursor.moveToNext())
        }
        return std
    }
}

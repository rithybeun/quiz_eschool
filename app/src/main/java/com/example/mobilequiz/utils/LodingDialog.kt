package com.example.mobilequiz.utils

import android.app.Activity
import android.app.AlertDialog
import com.example.mobilequiz.R

class LodingDialog(val mActivity: Activity)  {
    private lateinit var isDialog :AlertDialog
    fun startLoding(){
        //** Set View */
        val inflater = mActivity.layoutInflater
        val dialogView = inflater.inflate(R.layout.loading_widget,null)
        //** Set Dialog */
        val builder = AlertDialog.Builder(mActivity)
        builder.setView(dialogView)
        builder.setCancelable(false)
        isDialog = builder.create()
        isDialog.show()
    }
    fun isDismiss(){
        isDialog.dismiss()
    }
}
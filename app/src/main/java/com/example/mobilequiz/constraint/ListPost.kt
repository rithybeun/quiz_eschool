package com.example.mobilequiz.constraint

import com.example.mobilequiz.R
import com.example.mobilequiz.models.NewFeedModel

class ListPost {
     var arrayList: ArrayList<NewFeedModel> = ArrayList()

    fun listPostFun(arrayList : ArrayList<NewFeedModel>) : ArrayList<NewFeedModel>{
        arrayList.add(NewFeedModel(R.drawable.a,"Dara","អាគារ",R.drawable.a,"10h" ,true ))
        arrayList.add(NewFeedModel(R.drawable.b,"Sovan","តើប្រសាទអង្គរវត្តកសាងនៅឆ្នាំណា? ដោយស្តេចអង្គណា?",0,"53mn",false))
        arrayList.add(NewFeedModel(R.drawable.c, "Narith","It's Beautiful", R.drawable.c,"2h",false))
        arrayList.add(NewFeedModel(R.drawable.d, "Veasna", "Loved",  R.drawable.d,"1h",false))
        arrayList.add(NewFeedModel(R.drawable.e,"Pheak","Technology",  0,"7h",false))
        arrayList.add(NewFeedModel(R.drawable.f,"Lyly","love",  R.drawable.f,"42mn",true))
        arrayList.add(NewFeedModel(R.drawable.g,"Vichet","It's Beautiful", R.drawable.g, "15mn",false))
        return arrayList
    }
}
enum class LoginStatus {
    Success, Empty, Incorrect, Init
}

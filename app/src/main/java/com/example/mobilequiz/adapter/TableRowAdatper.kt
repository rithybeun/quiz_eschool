package com.example.mobilequiz.models

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.example.mobilequiz.R

class TableRowAdapter() :
    RecyclerView.Adapter<TableRowAdapter.ViewHolder>() {
    private var onClickDeleteItem: ((StudentModels) -> Unit)? = null
    private var onClickUpdateItem: ((StudentModels) -> Unit)? = null
    private var stdList: ArrayList<StudentModels> = ArrayList()
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {

        val v: View = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.table_row_layout, viewGroup, false)


        return ViewHolder(v)
    }

    fun addStudentItem(items: ArrayList<StudentModels>) {
        this.stdList = items
    }

    fun setOnClickUpdateItems(callback: (StudentModels) -> Unit) {
        this.onClickUpdateItem = callback
    }

    fun setOnClickDeleteItems(callback: (StudentModels) -> Unit) {
        this.onClickDeleteItem = callback
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        val std = stdList[i]
        viewHolder.txtIDStudent.text = stdList[i].id.toString()
        viewHolder.txtNameStudent.text = stdList[i].nameStudent
        viewHolder.txtSexStudent.text = stdList[i].ageStudent
        viewHolder.txtSubjectStudent.text = stdList[i].subjectStudent
        viewHolder.btnDeletedStudent.setOnClickListener { this.onClickDeleteItem?.invoke(std) }
        viewHolder.btnEditStudent.setOnClickListener { this.onClickUpdateItem?.invoke(std) }
    }

    override fun getItemCount(): Int {
        return stdList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtIDStudent: TextView = itemView.findViewById(R.id.txtIDStudent)
        val txtNameStudent: TextView = itemView.findViewById(R.id.txtNameStudent)
        val txtSexStudent: TextView = itemView.findViewById(R.id.txtAgeStudent)
        val txtSubjectStudent: TextView = itemView.findViewById(R.id.txtSubject)
        val btnEditStudent: Button = itemView.findViewById(R.id.btnEditStudent)
        val btnDeletedStudent: Button = itemView.findViewById(R.id.btnDeleteStudent)
    }

}

package com.example.mobilequiz

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.ListView
import com.example.mobilequiz.constraint.ListPost
import com.example.mobilequiz.databinding.ActivityMainBinding
import com.example.mobilequiz.fragment.LoginActivity
import com.example.mobilequiz.models.MyAdapterNewFeed
import com.example.mobilequiz.models.NewFeedModel

class MainActivity : AppCompatActivity() {
    //    lateinit var pageView: ViewPager
//    lateinit var tabLayout: TabLayout
    private lateinit var binding: ActivityMainBinding

    //    private lateinit var newFeedArrayList : ArrayList<NewFeedModel>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val listView = findViewById<ListView>(R.id.listViewMainActivity)
        var list = mutableListOf<NewFeedModel>()
        var listPost : ListPost = ListPost()
        for(dd in listPost.listPostFun(listPost.arrayList)){
            list.add(dd)
        }

        val myListAdapter = MyAdapterNewFeed(this, R.layout.list_item, list)
        listView.adapter = myListAdapter

        val button: Button = findViewById(R.id.btnlogin)
        button.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }

}

